/**
 * \file client.c
 * \brief Aplicacion cliente.
 * \author Matias S. Flores
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include "parser.h"
#include "funciones.h"

int get_data(char* input, char* datos[]);
void print_socket(int fd);
void console(int sockfd, char* user, char* host, char* port);
int get_file(int sockfd, int puerto, char* buffer);

int main(int argc, char *argv[]) {
	int sockfd, argC;
	struct sockaddr_in serv_addr;
	struct hostent *server;
	char *argV[20], *datos[3], buffer[TAM];

	printf("***************************************************************\n");
	printf("**** Sistemas Operativos II - Trabajo Practico 1 - Sockets ****\n");
	printf("***************************************************************\n");

	while (1) {
		char *tmp;

		memset(buffer, '\0', TAM);
		printf("local$ ");
		tmp = fgets(buffer, TAM, stdin);

		if (tmp == NULL || !strcmp(buffer, "\n")) {
			continue;
		}

		argC = fromKeyboard(argV, buffer);

		if (!strcmp(argV[0], "desconectar")) {
			exit(0);
		} else if (!strcmp(argV[0], "connect")) {
			if (argC == 2) {
				if (!get_data(argV[1], datos)) {
					continue;
				}

				sockfd = socket(AF_INET, SOCK_STREAM, 0);
				server = gethostbyname(datos[1]);

				if (sockfd < 0) {
					perror("No se pudo abrir el socket");
					continue;
				}

				if (server == NULL) {
					printf("No se encuentra el host\n");
					continue;
				}

				memset((char *) &serv_addr, '0', sizeof(serv_addr));
				serv_addr.sin_family = AF_INET;
				bcopy((char *) server->h_addr,
						(char *) &serv_addr.sin_addr.s_addr, server->h_length);
				serv_addr.sin_port = htons(atoi(datos[2]));

				if (connect(sockfd, (struct sockaddr *) &serv_addr,
						sizeof(serv_addr)) < 0) {
					printf("El servidor no responde\n");
					continue;
				}

				read_socket(sockfd, buffer);

				if (!strcmp("Bienvenido", buffer)) {
					char clave[10];

					memset(clave, '\0', 10);
					printf("Ingrese su clave: ");
					fgets(clave, 9, stdin);
					memset(buffer, '\0', TAM);
					strcpy(buffer, "connect ");
					strncat(buffer, datos[0], strlen(datos[0]));
					strncat(buffer, " ", 1);
					strncat(buffer, clave, strlen(clave));
					write_socket(sockfd, buffer);
					read_socket(sockfd, buffer);

					if (!strcmp("conectado", buffer)) {
						console(sockfd, datos[0], datos[1], datos[2]);
						continue;
					}

					printf("Clave y/o usuario incorrecto(s)\n");
					continue;
				} else {
					printf("No se recibio mensaje de Bienvenida: %s", buffer);
					close(sockfd);
				}
			} else {
				printf(
						"Comando incorrecto\nUso: connect usuario@ip:[puerto]\n");
			}
		} else {
			printf("Comando desconocido\n");
		}
	}
}

/**
 * \brief Parsear datos de conexion.
 * \param input Texto de comando \"connect\".
 * \param datos Referencia al array de salida.
 * \return 0 en caso de error, 1 en caso de exito.
 */
int get_data(char* input, char* datos[]) {
	char *tmp = strdup(input);

	if (!strcmp(tmp, "")) {
		printf("Comando incorrecto 1\nUso: connect usuario@ip:[puerto]\n");
		return 0;
	}

	datos[0] = strtok(tmp, "@");
	if (datos[0] == NULL || !strcmp(datos[0], "")) {
		printf("Comando incorrecto 2\nUso: connect usuario@ip:[puerto]\n");
		return 0;
	}

	datos[1] = strtok(NULL, ":");
	if (datos[1] == NULL || !strcmp(datos[1], "")) {
		printf("Comando incorrecto 3\nUso: connect usuario@ip:[puerto]\n");
		return 0;
	}

	datos[2] = strtok(NULL, "\n");
	if (datos[2] == NULL || !strcmp(datos[2], "")) {
		datos[2] = "6020";
	}
	return 1;
}

/**
 * \brief Mostrar en pantalla lo recibido en un Socket, hasta la aparicion de \"++\".
 * \param fd Socket.
 */
void print_socket(int fd) {
	char buffer[TAM];
	int exit = 0;

	while (!exit) {
		ssize_t n;

		memset(buffer, '\0', TAM);
		n = read(fd, buffer, TAM);

		if (n < 0) {
			perror("print_socket: Error en lectura de socket");
			break;
		} else if (n == 0) {
			break;
		} else {
			if (!strcmp(&buffer[n - 2], "++")) {
				exit = 1;
				buffer[n - 2] = '\0';
			}
			printf("%s", buffer);
		}
	}
}

/**
 * \brief Una vez logueado el usuario, se ejecuta uan consola que envia los comandos al servidor.
 * \param sockfd Socket.
 * \param user Usuario logueado.
 * \param host Direccion o Nombre del servidor.
 * \param port Puerto del Servidor.
 */
void console(int sockfd, char* user, char* host, char* port) {
	int argC;
	char *argV[20], buffer[TAM];

	while (1) {
		char *tmp;

		printf("%s@%s:%s$ ", user, host, port);
		memset(buffer, '\0', TAM);
		tmp = fgets(buffer, TAM, stdin);

		if (tmp == NULL || !strcmp(buffer, "\n")) {
			continue;
		}

		tmp = strdup(buffer);
		argC = fromKeyboard(argV, tmp);

		if (!strcmp(argV[0], "descargar")) {
			if (argC == 2) {
				get_file(sockfd, 6021, buffer);
			} else {
				printf("Comando incorrecto\nUso: descargar nro_estacion\n");
			}
		} else if (!strcmp(argV[0], "desconectar")) {
			write_socket(sockfd, buffer);
			close(sockfd);
			return;
		} else {
			write_socket(sockfd, buffer);
			print_socket(sockfd);
		}
	}
}

/**
 * \brief Obtener archivo mediante UDP.
 * \param sockfd Socket para envio de comandos.
 * \param puerto Puerto local a utilizar.
 * \param buffer Referencia a un buffer para envio y recepcion.
 * \return 0 en caso de error, 1 en caso de exito.
 * \bug Enviar IP al servidor en lugar de Localhost
 */
int get_file(int sockfd, int puerto, char* buffer) {
	int fd, tamano_direccion, recibido = 0, argC, *tamano;
	char *filename, *argV[20];
	struct sockaddr_in serv_addr;
	FILE *f;

	tamano = malloc(sizeof(int));
	fd = socket(AF_INET, SOCK_DGRAM, 0);

	if (fd < 0) {
		perror("listen: Error en apertura de socket");
		exit(1);
	}

	memset(&serv_addr, 0, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons(puerto);
	memset(&(serv_addr.sin_zero), '\0', 8);

	if (bind(fd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
		perror("listen: Error en binding");
		exit(1);
	}

	printf("Socket disponible: %d\n", ntohs(serv_addr.sin_port));
	write_socket(sockfd, buffer);
	sprintf(buffer, "localhost %d", puerto);
	write_socket(sockfd, buffer);
	read_socket(sockfd, buffer);
	argC = fromKeyboard(argV, buffer);

	if (argC != 2) {
		printf("listen: Error al recibir datos de archivo");
		free(tamano);
		return 0;
	}

	filename = strdup(argV[0]);
	f = fopen(filename, "w");

	if (f == NULL) {
		perror("listen: No se pudo crear el archivo");
		exit(1);
	}

	if (sscanf(argV[1], "%d", tamano)) {
		printf("Tama�o de archivo: %d bytes\n", *tamano);
		tamano_direccion = sizeof(struct sockaddr);

		while (recibido < *tamano) {
			int n;

			memset(buffer, '\0', TAM);
			n = recvfrom(fd, buffer, TAM, 0, (struct sockaddr *) &serv_addr,
					(socklen_t *) &tamano_direccion);

			if (n < 0) {
				perror("listen: Error en lectura de socket");
				exit(1);
			}

			recibido += (int) n;
			fprintf(f, "%s", buffer);
		}
	} else {
		printf("listen: Error al recibir tama�o de archivo");
	}

	fclose(f);
	close(fd);
	printf("Total recibido: %d bytes\n", recibido);
	printf("Archivo recibido: %s\n", filename);
	print_socket(sockfd);
	free(tamano);

	return 1;
}
