/**
* @mainpage Inicio
Sistemas Operativos II - Trabajo Practico 1 - Sockets
* ===================
*
* @author Matias S. Flores (matias.flores@outlook.com)
* @date Abril, 2017
* @version 1
* @bug Falta mejorar manejo de archivo CSV ya se podr&iacute;an reducir las lecturas.
* @bug Enviar IP al servidor en lugar de Localhost al recibir un archivo.
*
* @brief Sistema cliente-Servidor para obtener informacion desde un archivo .CSV sobre estaciones hidrometeorol&oacute;gicas.
*
* @par INTRODUCCI&Oacute;N:
* En la Provincia de C&oacute;rdoba existen aproximadamente 400 estaciones hidrometeorol&oacute;gicas
autom&aacute;ticas (AWS por sus siglas en ingl&eacute;s), de distintas marcas,
modelos y configuraciones, que pertenecen a distintas redes, instituciones y
organismos, desparramadas por todo el territorio provincial. Cada AWS consta de una
serie de sensores (bar&oacute;metro, term&oacute;metro, sensor de radiaci&oacute;n solar, etc.), conectado
a un sistema de adquisici&oacute;n de datos, que toma medidas de los sensores (telemetr&iacute;a)
un per&iacute;odo determinado de tiempo.
* Se le solicita a la Escuela de Ingenieria en Computacion que dise&ntilde;e, implemente
y testee un software (desarrollado en C), que permita realizar la conexion, control y
transferencia de datos telemetricos entre el un cliente y el servidor que contiene los
datos de las AWS (arquitectura cliente servidor).
* El programa que corre en el Cliente debe permitir conectarse a al Servidor, de
forma segura (orientado a conexion), utilizando un puerto fijo (6020) y tomar un numero
de un puerto libre (aleatorio) de su sistema operativo.
Debe implementarse un sistema de autenticacion (usuario y password) de
acceso al servidor.
*
* @par INSTALACI&Oacute;N:
* Los siguientes son los comandos &uacute;tiles para el uso de la aplicaci&oacute;n:\n
*
* - "make clean"	--> Limpia para una nueva compilaci&oacute;n.
* - "make server"	--> Genera archivo "server".
* - "make client"	--> Genera archivo "client".
* - "./server"		--> Inicia el Servidor escuchando en el puerto 6020 de todas las IPs asociadas.
* - "./client"		--> Corre aplicac&oacute;n cliente que permite conectarse a un servidor.
*
* @par Comandos sin conexi&oacute;n:
* - desconectar: salir de la aplicacion.
* - connect usuario\@servidor[:puerto]: conectarse a un servidor (ip o dns) en un puerto determinado (defecto: 6020). Se le solicitara una clave.
*
* @par Comandos con conexi&oacute;n:
* - listar: muestra un listado de todas las estaciones que hay en el archivo,
y muestra de que sensores tiene datos.
* - descargar nro_estacion: descarga un archivo con todos los datos de
la estacion.
* - diario_precipitacion nro_estacion: muestra el acumulado diario de la variable
precipitacion de una estacion.
* - mensual_precipitacion nro_estacion: muestra el acumulado mensual de la
variable precipitacion de una estacion.
* - promedio variable: muestra el promedio de todas las muestras de la variable
de cada estacion..
* - desconectar: termina la sesion del usuario.
*/
