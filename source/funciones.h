/**
* \file funciones.h
* \brief Funciones compartidas por ambas aplicaciones.
* \author Matias S. Flores
*/
/**
* \brief Hace un split del texto en espacio.
* \param argv Referencia al array de salida con los fragmentos.
* \param input Texto a procesar.
* \return Cantidad de fragmentos.
*/
int fromKeyboard(char* argv[], char* input) {
	int counter = 0;
	char* tmp;

	if (!strcmp(&input[strlen(input) - 1], "\n")) {
		input[strlen(input) - 1] = '\0';
	}
	argv[0] = strtok(input, " ");
	for (counter = 1; counter < 20; counter++) {
		tmp = strtok(NULL, " ");
		if (tmp == NULL) {
			break;
		}
		argv[counter] = tmp;
	}
	return (counter);
}

/**
 * \brief Escribe en un socket.
 * \param fd Socket.
 * \param buffer Informacion a enviar.
 * \return Cantidad de bytes enviados.
 */
ssize_t write_socket(int fd, const void *buffer) {
	ssize_t n;

	n = write(fd, buffer, strlen(buffer));

	if (n < 0) {
		perror("write_socket: Error al escribir en Socket");
		exit(1);
	}
	return n;
}

/**
 * \brief Lee desde un socket.
 * \param fd Socket.
 * \param buffer Referencia al buffer de salida.
 * \return Cantidad de bytes leidos.
 */
ssize_t read_socket(int fd, void *buffer) {
	ssize_t n;

	memset(buffer, '\0', TAM);
	n = read(fd, buffer, TAM);

	if (n < 0) {
		perror("read_socket: Error en lectura de socket");
		exit(1);
	}
	return n;
}
