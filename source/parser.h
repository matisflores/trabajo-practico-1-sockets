/**
 * \file parser.h
 * \brief Logica de parseo del archivo CSV y obtencion de datos.
 * \author Matias S. Flores
 */

/**
 * \brief Tama&ntilde;o de buffer.
 */
#define TAM 1024

/**
 * \brief Separador de columnas.
 */
const char* SEPARATOR = ",";

/**
 * \brief Obtener una columna determinada desde una linea separada por SEPARATOR
 * \param line Linea de texto leido.
 * \param num Numero de columna.
 * \return Texto de la columna.
 * \see SEPARATOR
 */
char* get_field(char* line, int num) {
	char *tok, *tmp;
	int counter = 0;

	tmp = strdup(line);
	tok = strtok(tmp, SEPARATOR);

	if (tok != NULL && num > counter) {
		for (; counter < num; counter++) {
			tok = strtok(NULL, SEPARATOR);
			if (tok == NULL)
				break;
		}
	}

	return tok;
}

/**
 * \brief Obtener encabezados del archivo CSV.
 * \param file Archivo.
 * \param headers Referencia al array de salida.
 * \return Cantidad de encabezados encontrados.
 */
int get_headers(char* file, char* headers[]) {
	int counter = 0;
	char header[TAM];
	FILE *stream;

	stream = fopen(file, "r");
	memset(header, 0, TAM);

	for (counter = 0; counter < 3; counter++) {
		if (fgets(header, TAM, stream) == NULL) {
			perror("Parser - No se pudo leer el archivo");
			exit(1);
		}
	}

	header[strlen(header) - 2] = '\0';
	headers[0] = strtok(header, SEPARATOR);

	for (counter = 1; counter < 25; counter++) {
		char *tok;

		tok = strtok(NULL, SEPARATOR);

		if (tok == NULL)
			break;

		headers[counter] = strdup(tok);
	}

	fclose(stream);

	return (counter);
}

/**
 * \brief Obtener las distintas estaciones existentes dentro del archivo CSV.
 * \param file Archivo.
 * \param stations Referencia al array de salida con Nro de Estacion.
 * \param names Referencia al arrat de salida con Nombre de Estacion.
 * \return Cantidad de estaciones encontradas.
 */
int get_stations(char* file, char* stations[], char* names[]) {
	int counter;
	char *last_station, *station, *station_name, line[TAM];
	FILE *stream;

	stream = fopen(file, "r");
	memset(line, 0, TAM);

	for (counter = 0; counter < 3; counter++) {
		if (fgets(line, TAM, stream) == NULL) {
			perror("Parser - No se pudo leer el archivo");
			exit(1);
		}
	}

	station = strdup(line);
	last_station = strdup(line);
	last_station[0] = '\0';
	counter = 0;

	while (fgets(line, TAM, stream) != NULL) {
		station = get_field(line, 0);

		if (station == NULL) {
			break;
		}

		if (strcmp(station, last_station)) {
			if (!strcmp(station, "\r\n")) {
				break;
			}

			if (!strcmp(station, "\n")) {
				break;
			}

			station_name = get_field(line, 1);
			stations[counter] = station;
			names[counter] = station_name;
			last_station = strdup(station);
			counter++;
		}
	}
	fclose(stream);

	return counter;
}

/**
 * \brief Obtener sensores que poseen datos para una Estacion.
 * \param file Archivo.
 * \param station Nro de Estacion.
 * \param sensors Referencia al array de salida.
 * \return Cantidad de sensores encontrados.
 */
int get_station_sensors(char* file, char* station, char* sensors[]) {
	int vars_c, vars_qty, counter = 0;
	char *vars_tmp, *vars[20], line[TAM];
	FILE *stream;

	vars_qty = get_headers(file, vars);
	stream = fopen(file, "r");
	memset(line, 0, TAM);

	for (counter = 0; counter < 3; counter++) {
		if (fgets(line, TAM, stream) == NULL) {
			perror("Parser - No se pudo leer el archivo");
			exit(1);
		}
	}

	counter = 0;

	while (fgets(line, TAM, stream) != NULL) {
		char *station_current;

		station_current = get_field(line, 0);

		if (station_current == NULL) {
			break;
		}

		if (!strcmp(station, station_current)) {
			for (vars_c = 0; vars_c < vars_qty; vars_c++) {
				vars_tmp = get_field(line, vars_c);

				if (strcmp(vars_tmp, "--")) {
					sensors[counter] = vars[vars_c];
					counter++;
				}
			}
			fclose(stream);

			return counter;
		}
	}
	fclose(stream);

	return counter;
}

/**
 * \brief Obtener un nuevo archivo CSV con datos solamente de una Estacion.
 * \param file Archivo.
 * \param station Nro de Estacion.
 * \return Nombre del archivo creado.
 */
char* make_station_file(char* file, char* station) {
	int counter = 0;
	char *file_name, line[TAM];
	FILE *stream, *f;

	stream = fopen(file, "r");
	file_name = "file.csv";
	memset(line, 0, TAM);
	f = fopen(file_name, "w");

	if (f == NULL) {
		perror("Parser - No se pudo crear el archivo");
		exit(1);
	}

	for (counter = 0; counter < 3; counter++) {
		if (fgets(line, TAM, stream) == NULL) {
			perror("Parser - No se pudo leer el archivo");
			exit(1);
		}

		fprintf(f, "%s", line);
	}

	counter = 0;

	while (fgets(line, TAM, stream) != NULL) {
		char *station_current;

		station_current = get_field(line, 0);

		if (station_current == NULL) {
			break;
		}

		if (!strcmp(station, station_current)) {
			fprintf(f, "%s", line);
		}
	}

	fclose(f);
	fclose(stream);

	return file_name;
}

/**
 * \brief Obtener promedios de variables para una determinada Estacion.
 * \param file Archivo.
 * \param station Nro de Estacion.
 * \param averages Referencia al array de salida.
 * \return Cantidad de promedios calculados.
 */
int get_station_averages(char* file, char* station, double* averages[]) {
	int counter = 0, founded = 0, avg_counter[20], vars_qty, vars_c;
	double *tmp;
	char *vars_tmp, *vars[20], line[TAM];
	FILE *stream;

	stream = fopen(file, "r");
	tmp = malloc(sizeof(double));
	vars_qty = get_headers(file, vars);
	memset(line, 0, TAM);

	for (counter = 0; counter < 3; counter++) {
		if (fgets(line, TAM, stream) == NULL) {
			perror("Parser - No se pudo leer el archivo");
			exit(1);
		}
	}

	for (counter = 0; counter < vars_qty; counter++) {
		averages[counter] = malloc(sizeof(double));
		avg_counter[counter] = 0;
	}

	while (fgets(line, TAM, stream) != NULL) {
		char *station_current;

		station_current = get_field(line, 0);

		if (station_current == NULL) {
			break;
		}

		if (!strcmp(station, station_current)) {
			founded = 1;

			for (vars_c = 0; vars_c < vars_qty; vars_c++) {
				vars_tmp = get_field(line, vars_c);

				if (strcmp(vars_tmp, "--")) {
					if (sscanf(vars_tmp, "%lf", tmp)) {
						*averages[vars_c] += *tmp;
						avg_counter[vars_c]++;
					}
				}
			}
		} else if (founded) {
			break;
		}
	}

	for (counter = 0; counter < vars_qty; counter++) {
		if (avg_counter[counter] > 0)
			*averages[counter] = *averages[counter] / avg_counter[counter];
	}
	free(tmp);
	fclose(stream);

	return vars_qty;
}

/**
 * \brief Obtener promedio de precipitacion diara de una Estacion.
 * \param file Archivo.
 * \param station Nro de Estacion.
 * \param days Referencia al array de salida con dias.
 * \param averages Referencia al array de salida con promedios.
 * \return Cantidad de promedios calculados.
 */
int get_station_daily_precipitation(char* file, char* station, char* days[],
		double* averages[]) {
	int counter = 0, founded = 0, avg_counter[31], days_c;
	double *tmp;
	char *vars_tmp, *day, *date_tmp, *last_day = "", line[TAM];
	FILE *stream;

	stream = fopen(file, "r");
	tmp = malloc(sizeof(double));
	memset(line, 0, TAM);

	for (counter = 0; counter < 3; counter++) {
		if (fgets(line, TAM, stream) == NULL) {
			perror("Parser - No se pudo leer el archivo");
			exit(1);
		}
	}

	days_c = -1;

	while (fgets(line, TAM, stream) != NULL) {
		char *station_current;

		station_current = get_field(line, 0);

		if (station_current == NULL) {
			break;
		}

		if (!strcmp(station, station_current)) {
			founded = 1;
			date_tmp = get_field(line, 3);
			day = strtok(date_tmp, "/");

			if (strcmp(day, last_day)) {
				days_c++;
				averages[days_c] = malloc(sizeof(double));
				avg_counter[days_c] = 0;
				days[days_c] = strtok(get_field(line, 3), " ");
				last_day = strdup(day);
			}

			vars_tmp = get_field(line, 7);

			if (strcmp(vars_tmp, "--")) {
				if (sscanf(vars_tmp, "%lf", tmp)) {
					*averages[days_c] += *tmp;
					avg_counter[days_c]++;
				}
			}
		} else if (founded) {
			break;
		}
	}

	days_c++;

	for (counter = 0; counter < days_c; counter++) {
		if (avg_counter[counter] > 0)
			*averages[counter] = *averages[counter] / avg_counter[counter];
	}
	fclose(stream);
	free(tmp);

	return days_c;
}

/**
 * \brief Obtener promedio de precipitacion mensual de una Estacion.
 * \param file Archivo.
 * \param station Nro de Estacion.
 * \param months Referencia al array de salida con meses.
 * \param averages Referencia al array de salida con promedios.
 * \return Cantidad de promedios calculados.
 */
int get_station_monthly_precipitation(char* file, char* station, char* months[],
		double* averages[]) {
	int counter = 0, founded = 0, avg_counter[31], months_c;
	double *tmp;
	char *vars_tmp, *date_tmp, *month, *last_month = "", line[TAM];
	FILE* stream;

	stream = fopen(file, "r");
	tmp = malloc(sizeof(double));
	memset(line, 0, TAM);

	for (counter = 0; counter < 3; counter++) {
		if (fgets(line, TAM, stream) == NULL) {
			perror("Parser - No se pudo leer el archivo");
			exit(1);
		}
	}

	months_c = -1;

	while (fgets(line, TAM, stream) != NULL) {
		char *station_current;

		station_current = get_field(line, 0);

		if (station_current == NULL) {
			break;
		}

		if (!strcmp(station, station_current)) {
			founded = 1;
			date_tmp = get_field(line, 3);
			strtok(date_tmp, "/");
			month = strtok(NULL, "/");

			if (strcmp(month, last_month)) {
				months_c++;
				averages[months_c] = malloc(sizeof(double));
				avg_counter[months_c] = 0;
				months[months_c] = strtok(get_field(line, 3), " ");
				last_month = strdup(month);
			}

			vars_tmp = get_field(line, 7);

			if (strcmp(vars_tmp, "--")) {
				if (sscanf(vars_tmp, "%lf", tmp)) {
					*averages[months_c] += *tmp;
					avg_counter[months_c]++;
				}
			}
		} else if (founded) {
			break;
		}
	}

	months_c++;

	for (counter = 0; counter < months_c; counter++) {
		if (avg_counter[counter] > 0)
			*averages[counter] = *averages[counter] / avg_counter[counter];
	}
	fclose(stream);
	free(tmp);

	return months_c;
}
