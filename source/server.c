/**
 * \file server.c
 * \brief Servidor de datos.
 * \author Matias S. Flores
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include "parser.h"
#include "funciones.h"

void console(int newsockfd);
int send_file(char* file, int newsockfd, char* buffer);

int main(int argc, char *argv[]) {
	int sockfd, clilen, argC;
	struct sockaddr_in serv_addr, cli_addr;
	char *argV[20], buffer[TAM];

	printf("***************************************************************\n");
	printf("**** Sistemas Operativos II - Trabajo Practico 1 - Sockets ****\n");
	printf("***************************************************************\n");

	clilen = sizeof(cli_addr);
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd < 0) {
		perror(" apertura de socket ");
		exit(1);
	}

	memset((char *) &serv_addr, 0, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons(6020);

	if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
		perror("Error");
		exit(1);
	}

	printf("Proceso: %d - socket disponible: %d\n", getpid(),
			ntohs(serv_addr.sin_port));

	listen(sockfd, 5);

	while (1) {
		int newsockfd, pid;

		newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr,
				(socklen_t *) &clilen);
		if (newsockfd < 0) {
			perror("Error al aceptar conexion");
			exit(1);
		}

		pid = fork();
		if (pid < 0) {
			perror("Error al hacer fork");
			exit(1);
		}

		if (pid == 0) {
			close(sockfd);
			write_socket(newsockfd, "Bienvenido");
			read_socket(newsockfd, buffer);
			argC = fromKeyboard(argV, buffer);

			if (!strcmp(argV[0], "connect") && argC == 3) {
				if (!strcmp(argV[1], "test") && !strcmp(argV[2], "123")) {
					printf("PROCESO %d. login correcto\n", getpid());
					write_socket(newsockfd, "conectado");
					console(newsockfd);
				} else {
					write_socket(newsockfd, "no conectado");
					printf("PROCESO %d. login incorrecto\n", getpid());
				}
			} else {
				write_socket(newsockfd, "no conectado");
				printf("PROCESO %d. login incorrecto\n", getpid());
			}
			exit(0);
		} else {
			printf("SERVIDOR: Nuevo cliente: %d\n", pid);
			close(newsockfd);
		}
	}
	return 0;
}

/**
 * \brief Consola virtual que interpreta los comandos enviados por el cliente.
 * \param newsockfd Socket con el cliente.
 */
void console(int newsockfd) {
	char *argV[20], *vars[20], *file, buffer[TAM];
	int vars_qty;

	file = "datos_meteorologicos.CSV";
	vars_qty = get_headers(file, vars);

	while (1) {
		int argC;

		read_socket(newsockfd, buffer);
		argC = fromKeyboard(argV, buffer);

		if (!strcmp(argV[0], "listar")) {
			char *stations[10], *names[10], *sensors[20];
			int i, j, qty;

			qty = get_stations(file, stations, names);

			for (i = 0; i < qty; i++) {
				int sensors_qty;

				sprintf(buffer, "%s - %s\n", stations[i], names[i]);
				write_socket(newsockfd, buffer);
				sensors_qty = get_station_sensors(file, stations[i], sensors);

				for (j = 4; j < sensors_qty; j++) {
					sprintf(buffer, "  - %s\n", sensors[j]);
					write_socket(newsockfd, buffer);
				}
			}
			sprintf(buffer, "Estaciones encontradas: %d\n", qty);
			write_socket(newsockfd, buffer);
		} else if (!strcmp(argV[0], "descargar")) {
			if (argC == 2) {
				char *archivo;

				archivo = make_station_file(file, argV[1]);
				printf(buffer, "PROCESO %d. Archivo creado: %s\n", getpid(),
						archivo);
				send_file(archivo, newsockfd, buffer);
			} else {
				sprintf(buffer,
						"Comando incorrecto\nUso: descargar nro_estacion\n");
				write_socket(newsockfd, buffer);
			}
		} else if (!strcmp(argV[0], "diario_precipitacion")) {
			if (argC == 2) {
				char *days[31];
				double *averages[31];
				int qty, j;

				qty = get_station_daily_precipitation(file, argV[1], days,
						averages);

				for (j = 0; j < qty; j++) {
					sprintf(buffer, "- %s: %.2f mm\n", days[j], *averages[j]);
					write_socket(newsockfd, buffer);
				}
			} else {
				sprintf(buffer,
						"Comando incorrecto\nUso: diario_precipitacion nro_estacion\n");
				write_socket(newsockfd, buffer);
			}
		} else if (!strcmp(argV[0], "mensual_precipitacion")) {
			if (argC == 2) {
				char *months[31];
				double *averages[31];
				int qty, j;

				qty = get_station_monthly_precipitation(file, argV[1], months,
						averages);

				for (j = 0; j < qty; j++) {
					sprintf(buffer, "- %s: %.2f mm\n", months[j], *averages[j]);
					write_socket(newsockfd, buffer);
				}

			} else {
				sprintf(buffer,
						"Comando incorrecto\nUso: mensual_precipitacion nro_estacion\n");
				write_socket(newsockfd, buffer);
			}
		} else if (!strcmp(argV[0], "promedio")) {
			if (argC == 2) {
				long ret;

				ret = strtol(argV[1], NULL, 10);

				if (ret >= 4 && ret < vars_qty) {
					char *stations[10], *names[10];
					double *averages[20];
					int qty, j;

					qty = get_stations(file, stations, names);
					sprintf(buffer, "Promedios de %s:\n", vars[ret]);
					write_socket(newsockfd, buffer);

					for (j = 0; j < qty; j++) {
						get_station_averages(file, stations[j], averages);

						sprintf(buffer, "- %s %s:  %.2f\n", stations[j],
								names[j], *averages[ret]);
						write_socket(newsockfd, buffer);
					}
				} else {
					int j;

					sprintf(buffer,
							"Comando incorrecto\nUso: promedio nro_variable\n");
					write_socket(newsockfd, buffer);
					sprintf(buffer, "Variables:\n");
					write_socket(newsockfd, buffer);

					for (j = 4; j < vars_qty; j++) {
						sprintf(buffer, " %d - %s\n", j, vars[j]);
						write_socket(newsockfd, buffer);
					}
				}
			} else {
				int j;

				sprintf(buffer,
						"Comando incorrecto\nUso: promedio nro_variable\n");
				write_socket(newsockfd, buffer);
				sprintf(buffer, "Variables:\n");
				write_socket(newsockfd, buffer);

				for (j = 4; j < vars_qty; j++) {
					sprintf(buffer, " %d - %s\n", j, vars[j]);
					write_socket(newsockfd, buffer);
				}
			}
		} else if (!strcmp(argV[0], "desconectar")) {
			printf("PROCESO %d. termino la ejecuci�n.\n", getpid());
			return;
		}
		write_socket(newsockfd, "++");
	}
}

/**
 * \brief Enviar un archivo mediante UDP
 * \param file Archivo a enviar.
 * \param newsockfd Socket con el cliente.
 * \param buffer Referencia a buffer para envio y recepcion.
 * \return 0 en caso de error, 1 en caso de exito.
 */
int send_file(char* file, int newsockfd, char* buffer) {
	int argC, tamano_direccion;
	char *argV[20];
	struct sockaddr_in dest_addr;

	tamano_direccion = sizeof(dest_addr);
	read_socket(newsockfd, buffer);
	argC = fromKeyboard(argV, buffer);

	if (argC != 2) {
		sprintf(buffer, "Comando incorrecto\nSe esperaba: host puerto\n");
		write_socket(newsockfd, buffer);
		return 0;
	} else {
		int fd, size, enviado = 0;
		struct hostent *server;
		FILE* stream;

		printf("PROCESO %d. enviar archivo %s a %s:%s\n", getpid(), file,
				argV[0], argV[1]);
		server = gethostbyname(argV[0]);
		if (server == NULL) {
			perror("send_file: Error no existe el host\n");
			exit(0);
		}

		fd = socket(AF_INET, SOCK_DGRAM, 0);
		if (fd < 0) {
			perror("send_file: Error en apertura de socket");
			exit(1);
		}

		dest_addr.sin_family = AF_INET;
		dest_addr.sin_port = htons(atoi(argV[1]));
		dest_addr.sin_addr = *((struct in_addr *) server->h_addr);
		memset(&(dest_addr.sin_zero), '\0', 8);

		stream = fopen(file, "r");
		if (stream == NULL) {
			perror("send_file: Error no se pudo abrir el archivo");
			exit(1);
		}

		fseek(stream, 0, SEEK_END);
		size = ftell(stream);
		rewind(stream);
		printf("PROCESO %d. Tama�o de archivo: %d bytes\n", getpid(), size);
		sprintf(buffer, "file3.csv %d", size);
		write_socket(newsockfd, buffer);

		while (fgets(buffer, TAM, stream) != NULL) {
			int n;

			n = sendto(fd, buffer, (int) strlen(buffer), 0,
					(struct sockaddr *) &dest_addr, tamano_direccion);
			if (n < 0) {
				perror("send_file: Error en escritura en socket");
				exit(1);
			}
			memset(buffer, '\0', TAM);
			enviado += (int) n;
		}
		fclose(stream);
		printf("PROCESO %d. Total enviado: %d bytes\n", getpid(), enviado);

		return 1;
	}
}
