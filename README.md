Trabajo	Práctico	1	- Sockets

En la Provincia de Córdoba existen aproximadamente 400 estaciones hidrometeorológicas
automáticas (AWS por sus siglas en inglés), de distintas marcas,
modelos y configuraciones, que pertenecen a distintas redes, instituciones y
organismos, desparramadas por todo el territorio provincial. Cada AWS consta de una
serie de sensores (barómetro, termómetro, sensor de radiación solar, etc.), conectado
a un sistema de adquisición de datos, que toma medidas de los sensores (telemetría)
un período determinado de tiempo. 